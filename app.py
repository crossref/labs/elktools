import streamlit as st
from streamlit_extras.dataframe_explorer import dataframe_explorer
import datetime
import utils.elkdump as elkdump
import utils.analysis as analysis
import pandas as pd
from collections import Counter
from joblib import Memory
import numpy as np
import matplotlib.pyplot as plt
import humanize

elk_cache_location = "./elk_cache"
memory = Memory(elk_cache_location, verbose=0)

LOTS = 1000
MINTIME = datetime.time(0, 0, 0)
# MAXTIME = datetime.time(23, 59, 59)
# For testing set maxime to MINTIME + 1 minute
MAXTIME = datetime.time(0, 5, 0)
# DEFAULT_QUERY = "backend_name:rest-api-polite-aws"
# DEFAULT_QUERY = "backend_name:rest-api-internal-aws"
DEFAULT_QUERY = 'http_request:"iPage?doi=*"'
DEFAULT_QUERY = 'http_request:"coaccess.html?doi*"'
DEFAULT_QUERY = 'http_request:"iPage?doi=*" OR http_request:"coaccess.html?doi*"'
# DEFAULT_QUERY = 'http_request:"openurl*"'
ESSENTIAL_COLUMNS = [
    "@timestamp",
    "client",
    "http_verb",
    "http_request",
    "time_duration",
    "http_status_code",
    "captured_user_agent",
    "combined_parameters",
    # "frontend_name",
    # "backend_name",
]

PROGRESS_TEXT = "Retrieving results..."


def start_datetime():
    return datetime.datetime.combine(
        st.session_state.start_date, st.session_state.start_time
    )


def end_datetime():
    return datetime.datetime.combine(
        st.session_state.end_date, st.session_state.end_time
    )


def sense_check():
    query = st.session_state.query
    count = elkdump.result_count(start_datetime(), end_datetime(), query)
    total_count = elkdump.result_count(start_datetime(), end_datetime(), "*")
    st.session_state["result_count"] = count
    st.session_state["total_count"] = total_count


def get_elk_results(start_dt, end_dt, query, count):
    my_bar = st.progress(0, text=PROGRESS_TEXT)
    df = pd.DataFrame()
    retrieved = 0
    all_records = []
    for res in elkdump.multi_results(start_dt, end_dt, query, threads=10):
        for _, r in res:
            retrieved += 1
            d = r["_source"]
            all_records.append(d)
            my_bar.progress(retrieved / count, f"Retrieving results {retrieved:,}")
    my_bar.progress(1.0, text="Done!")
    my_bar.empty()
    df = pd.json_normalize(all_records)
    return df


get_results_cached = memory.cache(get_elk_results)


def init_sidebar():
    with st.sidebar:
        st.title("Sidebar")
        st.date_input("Start date", key="start_date")
        st.time_input(
            "Start time",
            value=MINTIME,
            key="start_time",
            step=datetime.timedelta(minutes=1),
        )
        st.date_input("End date", key="end_date")
        st.time_input(
            "End time",
            value=MAXTIME,
            key="end_time",
            step=datetime.timedelta(minutes=1),
        )

        st.text_input("Query", key="query", value=DEFAULT_QUERY)
        # if st.session_state.result_count == 0:
        st.button("Count matching records", on_click=sense_check)


def show_splash():
    st.title("Welcome to the ELK request analyzer")
    st.header("This is a Crossref Labs project. It will likely headbutt you.")
    st.markdown("[![Click me](app/static/elk.png)](https://crossref.org/labs)")


def bar_chart(df, x, y, title, xlabel, ylabel):
    fig = plt.figure(figsize=(24, 5))
    fig.autofmt_xdate()
    y_pos = np.arange(len(df[x]))
    # plt.bar(y_pos, df[y], align="center", alpha=0.5)
    plt.bar(y_pos, df[y])
    # plt.xticks(y_pos, df[x])
    plt.xticks(y_pos, df[x], rotation=45)
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    st.pyplot(fig)


def plot_results(df, x, y, title, xlabel, ylabel):
    fig = plt.figure(figsize=(24, 5))
    fig.autofmt_xdate()
    plt.plot(df[x], df[y])
    plt.xticks(rotation=45)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    st.pyplot(fig)


def show_error_rates(df):
    with st.expander("Error rates"):
        client_error_count = df["http_status_code"].between(400, 499).sum()
        st.write(f"Client error count: {client_error_count:,}")
        client_error_rate = client_error_count / len(df)
        st.write(f"Client error rate: {client_error_rate:.2%}")

        server_error_count = df["http_status_code"].between(500, 599).sum()
        st.write(f"Server error count: {server_error_count:,}")
        server_error_rate = server_error_count / len(df)
        st.write(f"Server error rate: {server_error_rate:.2%}")

        total_error_count = df["http_status_code"].between(400, 599).sum()
        st.write(f"Total Error count: {total_error_count:,}")
        error_rate = total_error_count / len(df)
        st.write(f"Total Error rate: {error_rate:.2%}")


def show_requests_per_second(df):
    with st.expander("Requests per second"):
        rps = Counter(analysis.timestamps(df))
        rps_df = pd.DataFrame.from_dict(rps, orient="index", columns=["count"])

        mean_count = rps_df["count"].mean()
        st.write(f"Mean: {mean_count:.2f}")

        max_count = rps_df["count"].max()
        st.write(f"Maximum: {max_count:,}")

        standard_deviation = rps_df["count"].std()
        st.write(f"Standard Deviation: {standard_deviation:.2f}")
        # create a new column with the index
        rps_df["timestamp"] = rps_df.index
        st.dataframe(rps_df)

        plot_results(
            rps_df,
            "timestamp",
            "count",
            "Requests per second",
            "Time",
            "Requests per second",
        )
        # bar_chart(
        #     rps_df,
        #     "timestamp",
        #     "count",
        #     "Requests per second",
        #     "Time",
        #     "Requests per second",
        # )


def show_response_times(df):
    with st.expander("Response times"):
        mean_duration = df["time_duration"].mean()
        st.write(f"Mean: {mean_duration:.2f}")

        max_duration = df["time_duration"].max()
        st.write(f"Maximum: {max_duration:,}")

        standard_deviation = df["time_duration"].std()
        st.write(f"Standard Deviation: {standard_deviation:.2f}")

        # calculate the average response time per second
        df["@timestamp"] = pd.to_datetime(df["@timestamp"])
        df = df.groupby(df["@timestamp"])["time_duration"].mean().reset_index()
        st.dataframe(df)
        plot_results(
            df,
            "@timestamp",
            "time_duration",
            "Response Times",
            "Time",
            "Response Time (ms)",
        )


def show_top_users(df):
    with st.expander("To 10 IPs (clients)"):
        top_ten_clients = Counter(analysis.clients(df)).most_common(10)
        top_ten_clients_df = pd.DataFrame(top_ten_clients, columns=["client", "count"])
        # add a column with percentage of requests
        top_ten_clients_df["percentage"] = top_ten_clients_df["count"] / len(df)
        # format the percentage column
        top_ten_clients_df["percentage"] = top_ten_clients_df["percentage"].map(
            "{:,.2%}".format
        )
        # add a column with the count of unique user-agents for the client ip
        top_ten_clients_df["unique_user_agents"] = top_ten_clients_df["client"].map(
            lambda x: analysis.unique_user_agents_count(df, x)
        )
        # add a column with the most frequent user-agent for the client ip
        top_ten_clients_df["most_frequent_user_agent"] = top_ten_clients_df[
            "client"
        ].map(lambda x: analysis.most_frequent_user_agent(df, x))
        # add geolocation info if credentials exist
        if analysis.maxmind_credentials_exist():
            top_ten_clients_df = analysis.add_geo_info(top_ten_clients_df)
        # show the dataframe
        st.dataframe(top_ten_clients_df)

    with st.expander("Top 10 user-agents"):
        top_ten_user_agents = Counter(analysis.user_agents(df)).most_common(10)
        top_ten_user_agents_df = pd.DataFrame(
            top_ten_user_agents, columns=["user_agent", "count"]
        )
        # add a column with percentage of requests
        top_ten_user_agents_df["percentage"] = top_ten_user_agents_df["count"] / len(df)
        # format the percentage column
        top_ten_user_agents_df["percentage"] = top_ten_user_agents_df["percentage"].map(
            "{:,.2%}".format
        )
        # add a column with the count of unique ips for the user-agent
        top_ten_user_agents_df["unique_ips"] = top_ten_user_agents_df["user_agent"].map(
            lambda x: analysis.unique_ips_count(df, x)
        )
        # add a column with the most frequent ip for the user-agent
        top_ten_user_agents_df["most_frequent_ip"] = top_ten_user_agents_df[
            "user_agent"
        ].map(lambda x: analysis.most_frequent_ip(df, x))
        # display the dataframe
        st.dataframe(top_ten_user_agents_df)


def show_combined_parameters(df):
    with st.expander("Most common combined parameters"):
        # counts of distinct values in compined parameters
        combined_parameters = Counter(df["combined_parameters"]).most_common(10)
        combined_parameters_df = pd.DataFrame(
            combined_parameters, columns=["combined_parameters", "count"]
        )
        # add a column with percentage of requests
        combined_parameters_df["percentage"] = combined_parameters_df["count"] / len(df)
        # format the percentage column
        combined_parameters_df["percentage"] = combined_parameters_df["percentage"].map(
            "{:,.2%}".format
        )
        st.dataframe(combined_parameters_df)


def show_results():
    st.title("Results")
    st.header("Overview")
    st.write(f"From **{start_datetime()}** to **{end_datetime()}**")
    humanized_time_delta = humanize.precisedelta(end_datetime() - start_datetime())
    st.write(f"(**{humanized_time_delta}**)")

    st.markdown(f"Query: **{st.session_state.query}**")
    st.write(
        f"Retrieved **{st.session_state.result_count:,}** results out of **{st.session_state.total_count:,}** total requests"
    )
    st.markdown(
        f"The retrieved sample accounts for **{st.session_state.result_count/st.session_state.total_count*100:.2f}%** of total requests for this period"
    )

    


    
    df = get_results_cached(
        start_datetime(),
        end_datetime(),
        st.session_state.query,
        st.session_state.result_count,
    )
    df.fillna("", inplace=True)
    df = analysis.add_combined_paramters_column(df)
    st.download_button("Download all retrieved results", df.to_csv(), "results.csv")
    st.header("Filter")
    filtered_df = dataframe_explorer(df[ESSENTIAL_COLUMNS])


    st.dataframe(filtered_df, use_container_width=True)
    

    requests = analysis.requests(filtered_df)
    clients = analysis.clients(filtered_df)
    unique_clients = set(clients)
    st.header("Analysis")
    st.write(
        f"Filtering {len(requests)} requests from {len(unique_clients)} unique clients"
    )

    show_error_rates(filtered_df)
    show_requests_per_second(filtered_df)
    show_response_times(filtered_df)
    show_top_users(filtered_df)
    show_combined_parameters(filtered_df)

    param_values = analysis.extract_param_values(requests)

    with st.expander("Paramater usage"):
        st.markdown(
            "High cardinality refers to a parameter that can have many possible values. "
        )
        stats = analysis.calculate_param_stats(param_values, len(requests))
        st.write(stats)

    # st.session_state.result_count = 0


st.set_page_config(layout="wide")
if not elkdump.elk_reacheable():
    st.error("Unable to reach the ELK cluster. Are you connected to the VPN?")
    st.stop()

if "init" not in st.session_state:
    st.session_state["result_count"] = 0
    st.session_state["show_results"] = False
    st.session_state["init"] = True

init_sidebar()

if st.session_state.result_count > 0:
    if st.session_state.show_results:
        show_results()
    else:
        st.header(f"Found {st.session_state.result_count:,} results")
        if st.session_state.result_count >= LOTS:
            st.warning("This is a lot of results, consider narrowing your search")

        st.button(
            f"Retrieve and analyze {st.session_state.result_count:,} matching results",
            on_click=lambda: st.session_state.update({"show_results": True}),
        )
else:
    show_splash()
