# Using these scripts through a proxy

If you are using this through a socks proxy to get to the data center, you need to set your socks proxy environment variable first

```
export ALL_PROXY="socks5://localhost:8123"
```

Or, if you want to route DNS through socks:

```
export ALL_PROXY="socks5h://localhost:8123"
```

