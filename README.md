# README



# ELK Tools

This is a Crossref Labs project. 

This is a tool to allow us to analyze haproxy logs from our ELK instance and do some simple analysis of the resulting export.

It is designed to help us gather data about our services as we audit them in order to decide which ones to sunset.

It will only work if you have VPN access to the data center.

## Install

 ```
 git clone git@gitlab.com:crossref/labs/elktools.git
 ```

```
cd elktools
```

```
python -m venv venv
````

```
. venv/bin/activate
```

```
pip install requirements -r requirements.txt
```

## Configure environment variables

The tools can use the Maxmind geoip database. In order for this to work, you must set the `MAXMIND_ACCOUNT_ID` and `MAXMIND_LICENSE_KEY` envornment variables.

The needed values for thse can be found in 1Password under the entry "MAXMIND API Credentials.

```
export MAXMIND_ACCOUNT_ID=<account_d>
export MAXMIND_LICENSE_KEY=<license_key>
```



## Running the web interface

Make sure you are connected to the data center VPN, then:

```
streamlit run app.py
```

If you are not connected to the VPN, the app will complain and exit.

The sidebar will allows you to select dates/times and enter a query. Note that the query only works with Lucene query syntax- it does **not work** with Kibana Query Language (KQL).

Click on the "Count matching records" button to get an inital count of the number of records that would have to be downloaded. If you are happy with the quantity the in result set, click on the "Retrieve and analyze" button.

## Using the CLI

```
Usage: elk-dump.py [OPTIONS] OUTPUT_FILE

Arguments:
  OUTPUT_FILE  [required]

Options:
  --query-file FILE
  --start-date [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S]
                                  Start date  [default: 2023-06-13]
  --end-date [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S]
                                  End date  [default: 2023-06-13]
  --query TEXT                    Query string
  --help                          Show this message and exit.
```




