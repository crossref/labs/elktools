import json
import os
import logging
from collections import Counter, defaultdict
from pathlib import Path
from urllib.parse import parse_qs, urlparse

import pandas as pd
import rich
import typer
from rich import pretty
#from rich_dataframe import prettify
import math
from datetime import datetime

# import geoip2.database
import geoip2.webservice
from joblib import Memory
import matplotlib.pyplot as plt

geoip2_cache_location = "./geoip2_cache"
memory = Memory(geoip2_cache_location, verbose=0)


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def timestamps(elk_dump_df):
    return sorted(
        [row["@timestamp"].replace(microsecond=0) for _, row in elk_dump_df.iterrows()]
    )


def requests(elk_dump_df):
    return [
        row["http_request"]
        for _, row in elk_dump_df.iterrows()
        if isinstance(row["http_request"], str)
    ]


def clients(elk_dump_df):
    return [
        row["client"]
        for _, row in elk_dump_df.iterrows()
        if isinstance(row["client"], str)
    ]


def user_agents(elk_dump_df):
    return [
        row["captured_user_agent"]
        for _, row in elk_dump_df.iterrows()
        if isinstance(row["captured_user_agent"], str)
    ]


def most_frequent_user_agent(df, ip):
    return df[df["client"] == ip]["captured_user_agent"].value_counts().idxmax()


def most_frequent_ip(df, user_agent):
    return df[df["captured_user_agent"] == user_agent]["client"].value_counts().idxmax()


def unique_user_agents_count(df, ip):
    return len(df[df["client"] == ip]["captured_user_agent"].unique())


def unique_ips_count(df, user_agent):
    return len(df[df["captured_user_agent"] == user_agent]["client"].unique())


def add_geo_info(df):
    countries = []
    organizations = []
    is_anonymous_proxies = []
    try:
        for _, row in df.iterrows():
            if row["client"].startswith("172.20."):
                countries.append("US")
                organizations.append("Crossref")
                is_anonymous_proxies.append(False)
                continue

            ip_info = get_ip_info_cached(row["client"])

            countries.append(ip_info.country.iso_code)
            organizations.append(ip_info.traits.organization)
            is_anonymous_proxies.append(ip_info.traits.is_anonymous_proxy)
    except ValueError as e:
        logger.error(e)

    df["country"] = countries
    df["organization"] = organizations
    df["is_anonymous_proxy"] = is_anonymous_proxies

    return df


def maxmind_credentials_exist():
    ACCOUNT_ID = os.environ.get("MAXMIND_ACCOUNT_ID", None)
    LICENSE_KEY = os.environ.get("MAXMIND_LICENSE_KEY", None)
    if ACCOUNT_ID is None or LICENSE_KEY is None:
        logger.warning(
            "MAXMIND_ACCOUNT_ID and MAXMIND_LICENSE_KEY must be set as environment variables"
        )
        return False
    return True


def get_ip_info(ip):
    # set the ACCOUNT_ID and LICENSE_KEY from env variables
    try:
        ACCOUNT_ID = os.environ.get("MAXMIND_ACCOUNT_ID", None)
        LICENSE_KEY = os.environ.get("MAXMIND_LICENSE_KEY", None)
        with geoip2.webservice.Client(ACCOUNT_ID, LICENSE_KEY) as client:
            return client.city(ip)
    except geoip2.errors.AddressNotFoundError as e:
        logger.error(e)
        return None


get_ip_info_cached = memory.cache(get_ip_info)


def add_combined_paramters_column(df):
    combined_parameters = []
    for _, row in df.iterrows():
        url = urlparse(row["http_request"])
        query = parse_qs(url.query)
        combined_parameters.append(",".join(sorted(query.keys())))
    df["combined_parameters"] = combined_parameters
    return df


def extract_param_values(requests):
    param_values = defaultdict(list)
    for request in requests:
        url = urlparse(request)
        query = parse_qs(url.query)
        for key, value in query.items():
            param_values[key].append(f"{value}" if isinstance(value, list) else value)

    return param_values

def calculate_param_stats(param_values, total_requests):
    param_stats = defaultdict(list)
    for key, values in param_values.items():
        param_stats["parameter"].append(key)
        param_stats["coverage"].append(len(values) / total_requests)
        param_stats["unique-values"].append(len(set(values)))
        param_stats["cardinality"].append(len(set(values)) / total_requests)
        param_stats["most-frequent-value"].append(Counter(values).most_common(1)[0][0])

    df = pd.DataFrame(param_stats)
    df["coverage"] = df["coverage"].round(2)
    # format coverage as percentage
    df["coverage %"] = df["coverage"].apply(lambda x: f"{x*100:.0f}%")
    df["cardinality"] = df["cardinality"].round(2)
    # format cardinality as percentage
    df["cardinality %"] = df["cardinality"].apply(lambda x: f"{x*100:.0f}%")
    return df


def main(
    analysis_name: str = typer.Argument("elk_dump", help="Name of analysis"),
    elk_dump_path: Path = typer.Argument(..., help="Path to the elk dump file"),
):
    # Load the elk dump file

    with open(elk_dump_path, "r") as f:
        if elk_dump_path.suffix == ".jsonl":
            elk_dump = [json.loads(line) for line in f.readlines()]
        elif elk_dump_path.suffix == ".json":
            elk_dump = json.load(f)
        else:
            raise ValueError("Unknown file format")

    # Extract the query parameters from the elk dump

    param_values = extract_param_values(elk_dump)

    sorted_combined_param_values = extract_combined_param_values(elk_dump)

    # Compute some statistics on the query parameters

    param_stats = calculate_param_stats(param_values)


    param_stats_df = pd.DataFrame(param_stats)

    count_df = pd.DataFrame(
        Counter(sorted_combined_param_values).most_common(10),
        columns=["param-combination", "count"],
    )

    percentages = {
        item: count / len(sorted_combined_param_values)
        for item, count in dict(Counter(sorted_combined_param_values)).items()
    }
    percentages_df = pd.DataFrame(
        percentages.items(), columns=["param-combination", "percentage"]
    )
    param_stats_df.to_csv(f"{analysis_name}_param_stats.csv", index=False)
    count_df.to_csv(f"{analysis_name}_counts.csv", index=False)
    percentages_df.to_csv(f"{analysis_name}_percentages.csv", index=False)


if __name__ == "__main__":
    typer.run(main)
