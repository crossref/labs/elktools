import json
import logging
from datetime import datetime, time
from hashlib import sha256
from pathlib import Path
from typing import Optional

import pandas as pd
import requests
import typer
from elasticsearch import Elasticsearch  # , RequestsHttpConnection
from elasticsearch.helpers import scan
from joblib import Parallel, delayed
from rich.progress import Progress

ELASTIC_HOST = "logman2"
ELASTIC_PORT = 9200
ELASTIC_TIMEOUT = 60 * 10
PAGE_SIZE_FULL = 5000
INDEX = "haproxy-*"

# logging.basicConfig(level=logging.INFO)
# logger = logging.getLogger(__name__)


def get_es_connection():
    # return Elasticsearch(
    #     ELASTIC_HOST,
    #     port=ELASTIC_PORT,
    #     connection_class=RequestsHttpConnection,
    #     timeout=ELASTIC_TIMEOUT,
    # )
    return Elasticsearch(
        [f"http://{ELASTIC_HOST}:{ELASTIC_PORT}"],
        timeout=ELASTIC_TIMEOUT,
        # basic_auth=("elastic", "<>"),
        verify_certs=False,
    )


def elk_reacheable():
    try:
        host = f"http://{ELASTIC_HOST}:{ELASTIC_PORT}"
        requests.get(host)
        return True
    except requests.exceptions.ConnectionError as e:
        return False


def create_query(start_date: datetime, end_date: datetime, query: str) -> dict:
    return {
        "query": {
            "bool": {
                "must": [
                    {
                        "query_string": {
                            "query": query,
                            "analyze_wildcard": True,
                            "time_zone": "Etc/UTC",
                        }
                    }
                ],
                "filter": [
                    {
                        "range": {
                            "@timestamp": {
                                "format": "strict_date_optional_time",
                                "gte": start_date.strftime("%Y-%m-%dT%H:%M:%S"),
                                "lte": end_date.strftime("%Y-%m-%dT%H:%M:%S"),
                            }
                        }
                    }
                ],
                "should": [],
                "must_not": [],
            }
        },
    }


def result_count(start_date: datetime, end_date: datetime, query: str) -> int:
    # es = get_es_connection()
    # search_body = create_query(start_date, end_date, query)
    return get_es_connection().count(
        index=INDEX, body=create_query(start_date, end_date, query)
    )["count"]


def get_results(start_date: datetime, end_date: datetime, query: str) -> tuple:
    search_body = create_query(start_date, end_date, query)
    yield from enumerate(
        scan(get_es_connection(), query=search_body, index=INDEX, size=PAGE_SIZE_FULL)
    )


def end_of_day(dt: datetime) -> datetime:
    if dt.time() == time(0, 0):
        dt = datetime.combine(dt.date(), time(23, 59, 59, 999999))
    return dt


def split_into_equal_periods(
    start_date: datetime, end_date: datetime, number_of_periods: int
):
    duration = end_date - start_date
    typer.echo(f"Duration: {duration}")
    if duration.total_seconds() == 0:
        return [(start_date, end_date)]

    freq = duration // number_of_periods
    date_range = pd.date_range(start=start_date, end=end_date, freq=freq)
    date_range_tuples = list(zip(date_range, date_range[1:]))
    # add 1 second to the start of each period except the first to avoid overlap,
    # otherwise we double-count the first second of each period
    date_range_tuples[1:] = [
        (x[0] + pd.Timedelta(seconds=1), x[1]) for x in date_range_tuples[1:]
    ]
    return date_range_tuples


def foo(start_date: datetime, end_date: datetime, query: str):
    search_body = create_query(start_date, end_date, query)
    es = get_es_connection()
    yield from enumerate(scan(es, query=search_body, index=INDEX, size=PAGE_SIZE_FULL))


def multi_results(start_date: datetime, end_date: datetime, query: str, threads: int):
    periods = split_into_equal_periods(start_date, end_date, threads)
    yield from Parallel(n_jobs=threads, prefer="threads")(
        delayed(foo)(period[0], period[1], query) for period in periods
    )


def main(
    output_file: Path = typer.Argument(..., dir_okay=False),
    start_date: datetime = typer.Option(
        datetime.now().strftime("%Y-%m-%d"), help="Start date"
    ),
    end_date: Optional[datetime] = typer.Option(
        datetime.now().strftime("%Y-%m-%d"), help="End date"
    ),
    query: str = typer.Option(None, help="Query string"),
    threads: int = typer.Option(1, help="Number of threads"),
):
    search_body = create_query(start_date, end_date, query)

    total_results_count = es.count(index=INDEX, body=search_body)["count"]
    typer.echo(f"Total results: {total_results_count}")

    accumalted_results = []
    with open(output_file, "w") as f:
        for res in multi_results(start_date, end_date, query, threads):
            for r in res:
                f.write(json.dumps(r) + "\n")


if __name__ == "__main__":
    typer.run(main)
